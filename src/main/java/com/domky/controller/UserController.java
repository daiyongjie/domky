package com.domky.controller;

import com.domky.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping(value = "/findById")
    public User findById(@RequestParam(value = "id") int id) {
        System.out.println("id" + id);
        return new User();
    }

    @GetMapping(value = "/edit")
    public User findById(User user) {
        System.out.println("id" + user.getUsername());
        return new User();
    }

}
