package com.domky.model;

import lombok.Data;

@Data
public class User {
    private Integer id;

    private String username;
}
